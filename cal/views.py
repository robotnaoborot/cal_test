from datetime import timedelta

import django_filters
from django.utils.timezone import now
from django_filters.filters import _truncate
from rest_framework import viewsets, filters, pagination
from django_filters.rest_framework import DjangoFilterBackend, OrderingFilter
from rest_framework.generics import ListAPIView

from .serializers import EventSerializer, EventTypeSerializer
from .models import Event, EventType
from django.utils.translation import gettext_lazy as _


class EventFilterSet(django_filters.FilterSet):
    date = django_filters.DateRangeFilter()

    class Meta:
        model = Event
        fields = ['type', 'date']


class EventViewSet(viewsets.ModelViewSet):
    serializer_class = EventSerializer
    queryset = Event.objects.all()
    filter_backends = [DjangoFilterBackend, filters.SearchFilter]
    filter_class = EventFilterSet
    search_fields = ['title']
    pagination_class = pagination.PageNumberPagination


class EventTypeListView(ListAPIView):
    queryset = EventType.objects.all()
    serializer_class = EventTypeSerializer
